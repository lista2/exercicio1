import java.util.ArrayList;
public class Professor
{
    
    private int numeroIdentificacao;
    private String nome;
    private int escolaridade;
    private Disciplina[] disciplina;
    private ArrayList<Disciplina> disciplinaList;
    private double salario = 0;
    
    
    public Professor()
    {
      disciplina = new Disciplina[10];
      disciplinaList = new ArrayList<>();
    }
    
    public ArrayList<Disciplina> getDisciplinaList() {
        return disciplinaList;
    }
    
    public void addDisciplina(Disciplina disciplina) {
        this.disciplinaList.add(disciplina);
    }
    
    public int getNumeroIdentificacao() {
        return this.numeroIdentificacao;
    }
    
    public void setNumeroIdentificacao(int paramNumeroIdentificacao) {
        this.numeroIdentificacao = paramNumeroIdentificacao;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String paramNome) {
        this.nome = paramNome;
    }
    
    public int getEscolaridade() {
        return this.escolaridade;
    }
    
    public void setEscolaridade(int paramEscolaridade) {
        this.escolaridade = paramEscolaridade;
    }
    
    public double setSalario() {
    return this.salario;    
    }
        
    public double getSalario() {
        return this.salario;
    }
    
    public void calculaSalario() {
        double acrescimo;
        //double valor = 0;
        //Especializacao
        if(this.getEscolaridade() == 2) {
            acrescimo = 15;
        } 
        // Mestrado
        else if(this.getEscolaridade() == 3) {
            acrescimo = 45;
        } 
        // Doutorado
        else if (this.getEscolaridade() == 4){
            acrescimo = 75;
        } else {
            acrescimo = 0;    
        }
        
        for (int i = 0; i < disciplinaList.size(); i++) {
            Disciplina disciplina = disciplinaList.get(i);
            //System.out.println(disciplina.getValorHoraAula());
            double totalAcrescimo = (disciplina.getValorHoraAula() / 100) * acrescimo;
            double valor = disciplina.getValorHoraAula() + totalAcrescimo;
            
            if(disciplina.getTipoDisciplina() == 1) {                
                valor = valor - ((valor / 100) * 25);
                this.salario = this.salario + (valor * (disciplina.getCreditos() * 4));
            } else {
                this.salario = this.salario + (valor * (disciplina.getCreditos() * 4));
            }
        }        
    }
    
}
