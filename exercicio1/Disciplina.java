public class Disciplina
{
    
    private int id;
    private String nome;
    private int creditos;
    private int tipoDisciplina;
    private double valorHoraAula = 25.0;
    
    
    public Disciplina()
    {
        
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(int paramId) {
        this.id = paramId;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String paramNome) {
        this.nome = paramNome;
    }
    
    public int getCreditos() {
        return this.creditos;
    }
    
    public void setCreditos(int paramCreditos) {
        this.creditos = paramCreditos;
    }
    
    public int getTipoDisciplina() {
        return this.tipoDisciplina;
    }
    
    public void setTipoDisciplina(int paramTipoDisciplina) {
        this.tipoDisciplina = paramTipoDisciplina;
    }
    
    public double getValorHoraAula() {
        return this.valorHoraAula;
    }  

    
}
